if [ $(id -u) -eq 0 ];
then # you are root, make the prompt red
    PS1='$(if [[ $(pwd) == /jhub/_prod/* ]]; then p=$(pwd); p=${p#/jhub/_prod/}; p=${p#server_dadawebapps_}; echo "\[\e[0;31m\][$p]\[\e[m\]"; else echo "\[\e[0;31m\]\u@\h\[\e[m\]"; fi) \[\e[1;34m\]\w\[\e[m\] \[\e[0;31m\]\$ \[\e[m\]'
else
    PS1='$(if [[ $(pwd) == /jhub/_prod/* ]]; then p=$(pwd); p=${p#/jhub/_prod/}; p=${p#server_dadawebapps_}; echo "\[\e[0;32m\][$p]\[\e[m\]"; else echo "\[\e[0;32m\]\u@\h\[\e[m\]"; fi) \[\e[1;34m\]\w\[\e[m\] \[\e[0;32m\]\$ \[\e[m\]'
fi


