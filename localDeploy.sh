#!/bin/bash

LOCALDEPLOY=localDeploy.pl
SCRIPTSBASE=scripts-base.zip
SNAPVER=`curl -s http://artifactory.buongiorno.com/artifactory/buongiorno-snapshots/com/buongiorno/scripts-base/5.0-SNAPSHOT/maven-metadata.xml|grep -oP "(?<=<value>|<version>)[^<]+"|tail -n 1`
URLBASE=http://artifactory.buongiorno.com/artifactory/buongiorno-snapshots/com/buongiorno/scripts-base/5.0-SNAPSHOT/scripts-base-$SNAPVER.zip

if [ -f /tmp/$SCRIPTSBASE ]; then rm /tmp/$SCRIPTSBASE; fi

curl -sf -o /tmp/$SCRIPTSBASE $URLBASE || {
    echo "Cannot download client Helper. Exiting..."
    exit 1
}

if [ -f /tmp/$LOCALDEPLOY ]; then rm /tmp/$LOCALDEPLOY; fi

unzip -q -d /tmp /tmp/$SCRIPTSBASE $LOCALDEPLOY || {
        echo "Cannot unzip client from archive. Exiting..."
        exit 1
}

perl /tmp/$LOCALDEPLOY $@
